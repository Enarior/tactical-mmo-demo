Trying to learn game dev, my biggest project is this one. An MMO where players explore the world together and team up for turn-based combats.
Most of the code is ugly and this is a big scope(probably too big for me) but I actually managed to get a few things working.
Using Unity. 
Last version is playable here : https://simmer.io/@Enarior/~c1b8be41-aab9-6830-c8f0-f659679e92c7

### Things that are working:

 - Turn-based combat : movement, attack, item bar, team system, AI.
 - Animation : in both 3D/exploration and combat.
 - 3D movement and camera : kinda glitchy but working
 - 3D world : using a low-poly pack and Unity terrain system, ugly but working.
 - Game Menu : can start a game, leave game or manage settings.
 - Scene transition : from menu to game and then from 3D world to fight scene when an enemy is encountered. Music is different in all 3 scenes.

### Things that I need to do:
 - Implement more mechanics in fights.
 - Improve and grow 3D world(NPCs, quests, random ennemy generation, terrain).
 - In-game menu.
 - Save game feature.
 - Draw my own 3D art.

Even though this will probalby never be a fully-working project, I learned a crap ton of stuff working on it, and that's what matters.




















Don't watch too closely at my code or your eyes will bleed :-)
