﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterWorldAnimationController : MonoBehaviour
{
    Animator characterAnimator;
    int IsWalkingHash;
    int IsRunningHash;

    bool isWalking;
    bool isRunning;

    void Start()
    {
        characterAnimator = gameObject.GetComponent<Animator>();
        IsWalkingHash = Animator.StringToHash("IsWalking");
        IsRunningHash = Animator.StringToHash("IsRunning");
    }

    void Update()
    {
        //Check inputs and animator state
        isWalking = characterAnimator.GetBool(IsWalkingHash);
        isRunning = characterAnimator.GetBool(IsRunningHash);
        bool forwardPressed = Input.GetKey("z") || Input.GetKey("q") || Input.GetKey("d") || Input.GetKey("s");
        bool runningPressed = Input.GetKey("left shift");

        if (!isWalking && forwardPressed)
        {
            characterAnimator.SetBool(IsWalkingHash, true);
        }

		if (isWalking && !forwardPressed)
        {
            characterAnimator.SetBool(IsWalkingHash, false);
        }

        if(!isRunning && (forwardPressed && runningPressed))
		{
            characterAnimator.SetBool(IsRunningHash, true);
		}

        if(isRunning && (!forwardPressed || !runningPressed))
		{
            characterAnimator.SetBool(IsRunningHash, false);
        }
    }
}
