﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : TacticsMovement
{
    GameObject target;
    public GoblinFightAnimationController goblinAnimation;

    void Start()
    {
        goblinAnimation = gameObject.GetComponentInChildren<GoblinFightAnimationController>();
        move = 3;
        Init();
    }

    void Update()
    {
        if (!turn)
        {
            return;
        }

        if (!moving)
        {
            FindNearestTarget();
            CalculatePath();
            actualTargetTile.target = true;
            goblinAnimation.SetIsWalking(false);
        }
        else
        {
            Move();
            goblinAnimation.SetIsWalking(true);
        }

		if (moved && !attacked)
		{
            CheckForAttack();
            attacked = true;
            goblinAnimation.SetIsWalking(false);
        }

        if (moved && attacked)
        {
            TurnManager.EndTurn();
            attacked = false;
            moved = false;
            goblinAnimation.SetIsWalking(false);
        }
    }

    void CalculatePath()
    {
        Tile targetTile = GetTargetTile(target);
        FindPath(targetTile);
    }

    void FindNearestTarget()
    {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Player");

        GameObject nearest = null;
        float distance = Mathf.Infinity;

        foreach (GameObject obj in targets)
        {
            float d = Vector3.Distance(transform.position, obj.transform.position);

            if (d < distance)
            {
                distance = d;
                nearest = obj;
            }
        }

        target = nearest;
    }

    void CheckForAttack()
    {
        GameObject validTarget = nPCCombat.Attack();

        if (validTarget != null)
        {
            StartCoroutine(AttackCoroutine());
            CalculateHeading(validTarget.transform.position);
            transform.forward = new Vector3(heading.x, transform.forward.y, heading.z);
        }
    }

    IEnumerator AttackCoroutine()
    {
        goblinAnimation.SetIsAttacking(true);

        yield return new WaitForSeconds(3);

        goblinAnimation.SetIsAttacking(false);
    }
}
