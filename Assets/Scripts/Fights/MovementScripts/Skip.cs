﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skip : MonoBehaviour
{
	GameObject[] players;

	public void Start()
	{
		
	}

	public void SkipAttack()
	{
		players = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject player in players)
		{
            PlayerMovement pm = player.GetComponent<PlayerMovement>();
			if (pm.turn)
			{
                pm.SkipAttack();
			}
		}
	}

	public void SkipMovement()
	{
		players = GameObject.FindGameObjectsWithTag("Player");
		foreach (GameObject player in players)
		{
			PlayerMovement pm = player.GetComponent<PlayerMovement>();
			if (pm.turn)
			{
				pm.SkipMovement();
			}
		}
	}
}
