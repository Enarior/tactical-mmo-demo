﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : TacticsMovement
{
    public MouseManager mm;
    public CharacterFightAnimationController characterAnimation;

    void  Start()
    {
        characterAnimation = gameObject.GetComponentInChildren<CharacterFightAnimationController>();
        mm = gameObject.GetComponent<MouseManager>();
        move = 5;
        Init();
    }

    void Update()
    {
        if (!turn)
        {
            return;
        }

        if (!attacked && !moved && !moving)
        {
            characterAnimation.SetIsWalking(false);
            FindSelectableTiles();

            //Attack
            CheckForAttack();
            //Movement
            CheckMouse();
        }
        else if (moved && !attacked && !moving)
        {
            //Attack
            characterAnimation.SetIsWalking(false);
            FindSelectableTiles();
            RemoveSelectableTiles();

            CheckForAttack();
        }

        else if (!moved && attacked && !moving)
        {
            //Movement
            characterAnimation.SetIsWalking(false);
            FindSelectableTiles();
            RemoveAttackableTiles();
            CheckMouse();
        }

        else if (moving)
        {
            characterAnimation.SetIsWalking(true);
            Move();
        }

        else if (moved && attacked)
        {
            characterAnimation.SetIsWalking(false);
            TurnManager.EndTurn();
            moved = false;
            attacked = false;
        }
    }
    void CheckMouse()
	{
        if (Input.GetMouseButtonUp(0))
		{
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
			{
                if (hit.collider.tag == "Tile")
				{
                    Tile t = hit.collider.GetComponent<Tile>();

					if (t.selectable)
					{
                        MoveToTile(t);
					}
				}
			}
		}
	}

    void CheckForAttack()
	{
        if (Input.GetMouseButtonUp(0))
        {
            GameObject validTarget = playerCombat.Attack();

            if (validTarget != null)
            {
                StartCoroutine(AttackCoroutine());
                CalculateHeading(validTarget.transform.position);
                transform.forward = new Vector3(heading.x, transform.forward.y, heading.z);
            }
        }
    }
    IEnumerator AttackCoroutine()
    {
        attacked = true;
        characterAnimation.SetIsAttacking(true);

        yield return new WaitForSeconds(1.2f);

        characterAnimation.SetIsAttacking(false);
    }
}