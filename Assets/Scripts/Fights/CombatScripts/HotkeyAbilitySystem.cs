﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotkeyAbilitySystem
{
	public event EventHandler OnAbilityListChanged;
	public enum AbilityType
	{
		ConsumeHealthPotion,
		Jump,
		ChangePlayerColor,
		ConsumeManaPotion
	};

	private PlayerAbilities playerAbilities;
	private List<HotkeyAbility> hotkeyAbilityList;
	private List<HotkeyAbility> extraHotkeyAbilityList;


	public HotkeyAbilitySystem(PlayerCombat pc)
	{
		this.playerAbilities = pc.playerAbilities;
	    
		hotkeyAbilityList = new List<HotkeyAbility>();
		extraHotkeyAbilityList = new List<HotkeyAbility>();

		hotkeyAbilityList.Add(new HotkeyAbility
		{
			abilityType = AbilityType.ConsumeHealthPotion,
			activateAbilityAction = () => playerAbilities.ConsumeHealthPotion()
		});

		hotkeyAbilityList.Add(new HotkeyAbility
		{
			abilityType = AbilityType.Jump,
			activateAbilityAction = () => playerAbilities.Jump()
		});

		hotkeyAbilityList.Add(new HotkeyAbility
		{
			abilityType = AbilityType.ChangePlayerColor,
			activateAbilityAction = () => playerAbilities.ChangePlayerColor()
		});

		extraHotkeyAbilityList.Add(new HotkeyAbility
		{
			abilityType = AbilityType.ConsumeManaPotion,
			activateAbilityAction = () => playerAbilities.ConsumeManaPotion()
		});
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			hotkeyAbilityList[0].activateAbilityAction();
		}

		else if(Input.GetKeyDown(KeyCode.Alpha2))
		{
			hotkeyAbilityList[1].activateAbilityAction();
		}

		else if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			hotkeyAbilityList[2].activateAbilityAction();
		}
	}

	public List<HotkeyAbility> GetHotkeyAbililyList()
	{
		return hotkeyAbilityList;
	}

	public List<HotkeyAbility> GetExtraHotkeyAbililyList()
	{
		return extraHotkeyAbilityList;
	}

	public void SwapAbility(int abilityIndexA, int abilityIndexB)
	{
		HotkeyAbility tmp = hotkeyAbilityList[abilityIndexA];

		hotkeyAbilityList[abilityIndexA] = hotkeyAbilityList[abilityIndexB];
		hotkeyAbilityList[abilityIndexB] = tmp;
		OnAbilityListChanged?.Invoke(this, EventArgs.Empty);
	}

	public void SwapAbility(HotkeyAbility abilityA, HotkeyAbility abilityB)
	{
		if (extraHotkeyAbilityList.Contains(abilityA))
		{
			int indexB = hotkeyAbilityList.IndexOf(abilityB);
			hotkeyAbilityList[indexB] = abilityA;

			extraHotkeyAbilityList.Remove(abilityA);
			extraHotkeyAbilityList.Add(abilityB);
		} else {
			if (extraHotkeyAbilityList.Contains(abilityB))
			{
				int indexA = hotkeyAbilityList.IndexOf(abilityA);
				hotkeyAbilityList[indexA] = abilityB;

				extraHotkeyAbilityList.Remove(abilityB);
				extraHotkeyAbilityList.Add(abilityA);
			} else { 
				int indexA = hotkeyAbilityList.IndexOf(abilityA);
				int indexB = hotkeyAbilityList.IndexOf(abilityB);

				HotkeyAbility tmp = hotkeyAbilityList[indexA];

				hotkeyAbilityList[indexA] = hotkeyAbilityList[indexB];
				hotkeyAbilityList[indexB] = tmp;
			}
		}

		OnAbilityListChanged?.Invoke(this, EventArgs.Empty);
	}

	public class HotkeyAbility
	{
		public AbilityType abilityType;
		public Action activateAbilityAction;

		public Sprite GetSprite()
		{
			switch (abilityType)
			{
				default:
				case AbilityType.ConsumeHealthPotion: return PlayerAbilities.Instance.HealthPotionSprite;
				case AbilityType.Jump: return PlayerAbilities.Instance.JumpSprite;
				case AbilityType.ChangePlayerColor: return PlayerAbilities.Instance.ChangeColorSprite;
				case AbilityType.ConsumeManaPotion: return PlayerAbilities.Instance.ManaPotionSprite;
			}
		}
	}
}
