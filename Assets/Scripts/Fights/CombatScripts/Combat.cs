﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    public bool attacked;
    public int MaxHealth;
    public int currentHealth;
    public int damage;
    public int range;

    public MouseManager mm;
    public PlayerMovement playerMovement;
    public NPCMovement nPCMovement;
    public HealthBarScript healthBar;


    public void setHealth(int newHealth)
    {
        currentHealth = newHealth;
        healthBar.SetHealth(currentHealth);
    }
}
