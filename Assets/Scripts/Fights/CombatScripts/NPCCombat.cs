﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCCombat : Combat
{
    private GameObject[] targets;
    public PlayerCombat targetCombat;

    // Start is called before the first frame update
    void Start()
    {
       nPCMovement = gameObject.GetComponent<NPCMovement>();
       MaxHealth = 8;
       currentHealth = MaxHealth;
       healthBar.SetMaxHealth(MaxHealth);
       damage = 3;
       range = 1;
    }

    void Update()
    {
        if (currentHealth <= 0)
        {
            TurnManager.RemoveUnit(nPCMovement);
            nPCMovement.EndTurn();
            Destroy(gameObject);
        }

        if (currentHealth > MaxHealth)
        {
            setHealth(MaxHealth);
        }
    }

    public GameObject Attack()
    {
        targets = GameObject.FindGameObjectsWithTag("Player");
        GameObject target = targets[0];
        float dist = Mathf.Abs(transform.position.x - targets[0].transform.position.x) + Mathf.Abs(transform.position.z - targets[0].transform.position.z);

        //Find nearest target
        foreach (GameObject t in targets) {
            if(Mathf.Abs(transform.position.x - t.transform.position.x) + Mathf.Abs(transform.position.z - t.transform.position.z) < dist)
			{
                target = t;
			}
        }

        targetCombat = target.GetComponent<PlayerCombat>();
        
        //If a target is in range, attack
        if (Mathf.Abs(transform.position.x - target.transform.position.x) + Mathf.Abs(transform.position.z - target.transform.position.z) <= range) {
            targetCombat.setHealth(targetCombat.currentHealth - damage);
            return target;
        }
		else
		{
            return null;
		}
	}
}
