﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : Combat
{
    public HotkeyAbilitySystem hotkeyAbilitySystem;
    public NPCCombat targetCombat;
    public PlayerAbilities playerAbilities;
    public UI_Hotkey_Bar ui_Hotkey_Bar;

    public bool HealthPotionUsed = false;

    // Start is called before the first frame update
    void Start()
    {
        playerAbilities = gameObject.GetComponent<PlayerAbilities>();
        mm = gameObject.GetComponent<MouseManager>();
        playerMovement = gameObject.GetComponent<PlayerMovement>();
        hotkeyAbilitySystem = new HotkeyAbilitySystem(this);

        ui_Hotkey_Bar.setHotkeyAbilitySystem(hotkeyAbilitySystem);

        MaxHealth = 10;
        setHealth(MaxHealth);
        
        attacked = false;
        
        range = 2;
        damage = 2;
    }

    void Update()
    {
        hotkeyAbilitySystem.Update();

        if (currentHealth <= 0)
        {
            TurnManager.RemoveUnit(playerMovement);
            playerMovement.EndTurn();
            Destroy(gameObject);
        }

        if(currentHealth > MaxHealth)
		{
            setHealth(MaxHealth);
        }
    }

    public GameObject Attack()
    {
        GameObject target = mm.selectedObject;
        if (target != null && target.GetComponent<Combat>() != null && target.tag == "NPC" &&
            (Mathf.Abs(transform.position.x - target.transform.position.x) + Mathf.Abs(transform.position.z - target.transform.position.z) <= range))
        {
            targetCombat = mm.selectedObject.GetComponent<NPCCombat>();

            targetCombat.setHealth(targetCombat.currentHealth - damage);
            return mm.selectedObject;
        }
		else
		{
            return null;
		}
    }
}