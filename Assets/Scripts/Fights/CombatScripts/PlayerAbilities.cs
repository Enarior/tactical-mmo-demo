﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbilities : MonoBehaviour
{
	public static PlayerAbilities Instance { get; private set; }
	private TacticsMovement ActivePlayer;
	private PlayerCombat ActivePlayerCombat;

	public Sprite HealthPotionSprite;
	public Sprite JumpSprite;
	public Sprite ChangeColorSprite;
	public Sprite ManaPotionSprite;

	

	public void Awake()
	{
		Instance = this;
	}

	private void Update()
	{
		GetActivePlayer();
	}
	public void ConsumeHealthPotion()
	{
		if (ActivePlayerCombat != null && ActivePlayerCombat.HealthPotionUsed == false)
		{
			ActivePlayerCombat.setHealth(ActivePlayerCombat.currentHealth + 2);
			StartCoroutine(ChangeColorCoroutine(Color.green));
			ActivePlayerCombat.HealthPotionUsed = true;
		}
	}

	public void ConsumeManaPotion()
	{
		StartCoroutine(ChangeColorCoroutine(Color.blue));
	}

	public void Jump()
	{
		
	}

	public void ChangePlayerColor()
	{
		gameObject.GetComponent<Renderer>().material.color = new Color(Random.Range(0F, 1F), Random.Range(0, 1F), Random.Range(0, 1F));
	}


	private IEnumerator ChangeColorCoroutine(Color color)
	{
		Color oldColor = gameObject.GetComponent<Renderer>().material.color;

		gameObject.GetComponent<Renderer>().material.color = color;
		yield return new WaitForSeconds(0.2f);

		gameObject.GetComponent<Renderer>().material.color = oldColor;
	}

	private void GetActivePlayer()
	{
		if (TurnManager.turnTeam.Count > 0 && TurnManager.turnTeam.Peek().tag == "Player")
		{
			ActivePlayer = TurnManager.turnTeam.Peek();
			ActivePlayerCombat = ActivePlayer.playerCombat;
		}

		else
		{
			ActivePlayer = null;
			ActivePlayerCombat = null;
		}
	}
}