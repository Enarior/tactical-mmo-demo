﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseManager : MonoBehaviour
{
    public Camera cam;
    public GameObject selectedObject;
    public PlayerMovement pm;
    public Combat cm;
    public Combat playerCombat;

    // Initialization 
    void Start(){
        pm = gameObject.GetComponent<PlayerMovement>();
        cm = gameObject.GetComponent<PlayerCombat>();
    }

    // Update is called once per frame
    void Update(){
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo)) {
            //Debug.Log("Mouse is over: " + hitInfo.collider.name);
            GameObject hitObject = hitInfo.transform.root.gameObject;

            HoverObject(hitObject);
            if (Input.GetMouseButtonUp(0) && selectedObject.tag == "NPC" && Vector3.Distance(transform.position, selectedObject.transform.position) <= cm.range)
            {
                //Debug.Log("Attacking the NPC");
            }
        }
		else
		{
            ClearSelection();
		}
    }

    void HoverObject(GameObject obj){
        if(selectedObject != null){
            if (obj == selectedObject)
                return;
            ClearSelection();
		}
        selectedObject = obj;

        Renderer rend = selectedObject.GetComponent<Renderer>();

        if (rend != null && selectedObject.tag != "Player" && pm.turn && Vector3.Distance(transform.position, selectedObject.transform.position) <= cm.range )
		{
            rend.material.color = Color.red;
        }
    }

    void ClearSelection() {
        if (selectedObject == null)
            return;

        Renderer rend = selectedObject.GetComponent<Renderer>();
        if (rend != null && selectedObject.tag == "NPC")
        {
            rend.material.color = Color.yellow;
        }

        selectedObject = null;
    }
}
