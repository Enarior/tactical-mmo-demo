﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    static Dictionary<string, List<TacticsMovement>> units = new Dictionary<string, List<TacticsMovement>>();
    static Queue<string> turnKey = new Queue<string>();
    public static Queue<TacticsMovement> turnTeam = new Queue<TacticsMovement>();
    static LevelLoader levelLoader;

	public void Start()
	{
        levelLoader = GameObject.FindObjectOfType<LevelLoader>();
	}

	void Update()
    {
        if (turnTeam.Count == 0)
        {
            InitializeTeamTurnQueue();
        }

        foreach(KeyValuePair<string, List<TacticsMovement>> team in units)
		{
            if(team.Value.Count == 0)
			{
                levelLoader.LoadWorldScene();
			}
		}
    }

    static void InitializeTeamTurnQueue()
    {
        List<TacticsMovement> teamList = units[turnKey.Peek()];

        foreach (TacticsMovement unit in teamList)
        {
            turnTeam.Enqueue(unit);
        }

        StartTurn();
    }

    static void StartTurn()
    {
        if (turnTeam.Count > 0)
        {
            turnTeam.Peek().BeginTurn();
        }
    }

    public static void EndTurn()
    {
        TacticsMovement unit = turnTeam.Dequeue();
        unit.EndTurn();

        if (turnTeam.Count > 0)
        {
            StartTurn();
        }
        else
        {
            string team = turnKey.Dequeue();
            turnKey.Enqueue(team);
            InitializeTeamTurnQueue();
        }
    }

    public static void AddUnit(TacticsMovement unit)
	{
        List<TacticsMovement> list;

        if (!units.ContainsKey(unit.tag))
        {
            list = new List<TacticsMovement>();
            units[unit.tag] = list;

            if (!turnKey.Contains(unit.tag))
            {
                turnKey.Enqueue(unit.tag);
            }
        }
        else
        {
            list = units[unit.tag];
        }
        list.Add(unit);
	}

    public static void RemoveUnit(TacticsMovement unit)
	{
        units[unit.tag].Remove(unit);
	}
}