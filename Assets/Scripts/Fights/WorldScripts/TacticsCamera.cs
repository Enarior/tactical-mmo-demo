﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsCamera : MonoBehaviour
{
	bool rotatingRight = false;
	bool rotatingLeft = false;


	private void Update()
	{
		if (rotatingLeft)
		{
			transform.Rotate(Vector3.up, 1, Space.Self);
		}
		else if (rotatingRight)
		{
			transform.Rotate(Vector3.up, -1, Space.Self);
		}
	}

	public void startRotatingRight()
	{
		rotatingRight = true;
	}
	public void startRotatingLeft()
	{
		rotatingLeft = true;
	}

	public void stopRotating()
	{
		rotatingLeft = false;
		rotatingRight = false;
	}
}
