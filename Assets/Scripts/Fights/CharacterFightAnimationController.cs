﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterFightAnimationController : MonoBehaviour
{
    Animator characterAnimator;
    int IsWalkingHash;
    int IsAttackingHash;

    bool isWalking;
    bool isAttacking;

    void Start()
    {
        characterAnimator = gameObject.GetComponent<Animator>();
        IsWalkingHash = Animator.StringToHash("IsWalking");
        IsAttackingHash = Animator.StringToHash("IsAttacking");
    }

    void Update()
    {
		if (isWalking)
        {
            characterAnimator.SetBool(IsWalkingHash, true);
        }
		else
		{
            characterAnimator.SetBool(IsWalkingHash, false);
        }

		if (isAttacking)
		{
            characterAnimator.SetBool(IsAttackingHash, true);
		}
        else
        {
            characterAnimator.SetBool(IsAttackingHash, false);
        }
    }

    public void SetIsWalking(bool walking)
	{
        this.isWalking = walking;
	}

    public void SetIsAttacking(bool attacking)
    {
        this.isAttacking = attacking;
    }
}