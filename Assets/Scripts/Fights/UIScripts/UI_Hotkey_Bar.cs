﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Hotkey_Bar : MonoBehaviour
{
    private Transform abilitySlotTemplate;
	private HotkeyAbilitySystem hotkeyAbilitySystem;

	private void Awake()
	{
		abilitySlotTemplate = transform.Find("AbilitySlotTemplate");
		abilitySlotTemplate.gameObject.SetActive(false);
	}

	public void setHotkeyAbilitySystem(HotkeyAbilitySystem newHotkeyAbilitySystem)
	{
		hotkeyAbilitySystem = newHotkeyAbilitySystem;
		hotkeyAbilitySystem.OnAbilityListChanged += HotkeyAbilitySystem_OnAbilityListChanged;
		UpdateVisual();
	}

	private void HotkeyAbilitySystem_OnAbilityListChanged(object sender, System.EventArgs e )
	{
		UpdateVisual();
	}

	private void UpdateVisual()
	{
		//Clear old objects
		foreach (Transform child in transform)
		{
			if (child != abilitySlotTemplate)
			{
				Destroy(child.gameObject);
			}
		}

		List<HotkeyAbilitySystem.HotkeyAbility> hotkeyAbilityList = hotkeyAbilitySystem.GetHotkeyAbililyList();	

		for ( int i = 0; i < hotkeyAbilityList.Count; i++)
		{
			HotkeyAbilitySystem.HotkeyAbility hotkeyAbility = hotkeyAbilityList[i];
			Transform abilitySlotTransform = Instantiate(abilitySlotTemplate, transform);
			abilitySlotTransform.gameObject.SetActive(true);

			RectTransform abilitySlotRectTransform = abilitySlotTransform.transform.GetComponent<RectTransform>();
			abilitySlotRectTransform.anchoredPosition = new Vector2(60f * i , 0f );
			abilitySlotTransform.Find("HotkeyImage").GetComponent<Image>().sprite = hotkeyAbility.GetSprite();
			abilitySlotTransform.Find("numberText").GetComponent<Text>().text = (i + 1).ToString();

			abilitySlotTransform.GetComponent<UI_HotkeyBarAbilitySlot>().Setup(hotkeyAbilitySystem, i, hotkeyAbility);
		}

		hotkeyAbilityList = hotkeyAbilitySystem.GetExtraHotkeyAbililyList();

		//Extras
		for (int i = 0; i < hotkeyAbilityList.Count; i++)
		{
			HotkeyAbilitySystem.HotkeyAbility hotkeyAbility = hotkeyAbilityList[i];
			Transform abilitySlotTransform = Instantiate(abilitySlotTemplate, transform);
			abilitySlotTransform.gameObject.SetActive(true);

			RectTransform abilitySlotRectTransform = abilitySlotTransform.transform.GetComponent<RectTransform>();
			abilitySlotRectTransform.anchoredPosition = new Vector2(400f + 100f * i, 0f);
			abilitySlotTransform.Find("HotkeyImage").GetComponent<Image>().sprite = hotkeyAbility.GetSprite();
			abilitySlotTransform.Find("numberText").GetComponent<Text>().text = "";

			abilitySlotTransform.GetComponent<UI_HotkeyBarAbilitySlot>().Setup(hotkeyAbilitySystem, -1, hotkeyAbility);
		}
	}
}
