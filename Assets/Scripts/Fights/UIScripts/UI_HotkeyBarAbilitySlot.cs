﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_HotkeyBarAbilitySlot : MonoBehaviour, IDragHandler, IDropHandler, IBeginDragHandler, IEndDragHandler
{
	private RectTransform rectTransform;
	private Canvas canvas;
	private CanvasGroup canvasGroup;
	private HotkeyAbilitySystem.HotkeyAbility hotkeyAbility;
	private HotkeyAbilitySystem hotkeySystem;
	private int abilityIndex;
	private Vector2 startAnchoredPosition;

	private void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
		canvasGroup = gameObject.GetComponent<CanvasGroup>();

		Transform testCanvasTransform = transform;
		do
		{
			testCanvasTransform = testCanvasTransform.parent;
			canvas = testCanvasTransform.GetComponent<Canvas>();
		}
		while (canvas == null);
	}

	private void Start()
	{
		startAnchoredPosition = rectTransform.anchoredPosition;
	}

	public int getAbilityIndex()
	{
		return abilityIndex;
	}

	public HotkeyAbilitySystem.HotkeyAbility GetHotkeyAbility()
	{
		return this.hotkeyAbility;
	}

	public void Setup(HotkeyAbilitySystem newHotkeySystem, int newAbilityIndex, HotkeyAbilitySystem.HotkeyAbility newHotkeyAbility)
	{
		this.hotkeySystem = newHotkeySystem;
		this.abilityIndex = newAbilityIndex;
		this.hotkeyAbility = newHotkeyAbility;
	}

	public void OnDrop(PointerEventData eventData)
	{
		if( eventData.pointerDrag != null)
		{
			UI_HotkeyBarAbilitySlot uI_HotkeyBarAbilitySlot = eventData.pointerDrag.GetComponent<UI_HotkeyBarAbilitySlot>();
			if(uI_HotkeyBarAbilitySlot != null)
			{
				//hotkeySystem.SwapAbility(abilityIndex, uiHotkeyBarAbilitySlot.getAbilityIndex());
				hotkeySystem.SwapAbility(hotkeyAbility, uI_HotkeyBarAbilitySlot.GetHotkeyAbility());
			}
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		rectTransform.anchoredPosition = startAnchoredPosition;
		canvasGroup.alpha = 1f;
		canvasGroup.blocksRaycasts = true;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		canvasGroup.alpha = .6f;
		canvasGroup.blocksRaycasts = false;
		transform.SetAsLastSibling();
	}
}
