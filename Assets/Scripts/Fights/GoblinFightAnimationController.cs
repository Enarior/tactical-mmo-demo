﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoblinFightAnimationController : MonoBehaviour
{
    Animator goblinAnimator;
    int IsWalkingHash;
    int IsAttackingHash;

    bool isWalking;
    bool isAttacking;

    void Start()
    {
        goblinAnimator = gameObject.GetComponent<Animator>();
        IsWalkingHash = Animator.StringToHash("IsWalking");
        IsAttackingHash = Animator.StringToHash("IsAttacking");
    }

    void Update()
    {
        if (isWalking)
        {
            goblinAnimator.SetBool(IsWalkingHash, true);
        }
        else
        {
            goblinAnimator.SetBool(IsWalkingHash, false);
        }

        if (isAttacking)
        {
            goblinAnimator.SetBool(IsAttackingHash, true);
        }
        else
        {
            goblinAnimator.SetBool(IsAttackingHash, false);
        }
    }

    public void SetIsWalking(bool walking)
    {
        this.isWalking = walking;
    }

    public void SetIsAttacking(bool attacking)
    {
        this.isAttacking = attacking;
    }
}
