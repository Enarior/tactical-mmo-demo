﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public float TransitionTime;

    private GameObject[] players;
    private GameObject[] NPCs;

	private void Awake()
	{
        players = GameObject.FindGameObjectsWithTag("Player");
        NPCs = GameObject.FindGameObjectsWithTag("NPC");    
	}
	// Update is called once per frame
	void Update()
    {
		if (SceneManager.GetActiveScene().name == "World" && IsCloseToNPC())
		{
            LoadFightScene();
        }
    }

    public void LoadFightScene()
    {
        transition.SetBool("InFight", true);
        StartCoroutine(LoadFightSceneCoroutine());
    }

    IEnumerator LoadFightSceneCoroutine()
	{
        yield return new WaitForSeconds(2);

        SceneManager.LoadScene("Fight");
    }

    public void LoadWorldScene()
    {
        transition.SetBool("InFight", false);
        StartCoroutine(LoadWorldSceneCoroutine());
    }

    IEnumerator LoadWorldSceneCoroutine()
    {
        yield return new WaitForSeconds(1);

        SceneManager.LoadScene("World");
    }

    private bool IsCloseToNPC()
    {
        foreach (GameObject player in players)
        {
            foreach (GameObject NPC in NPCs)
            {
                if (Vector3.Distance(player.transform.position, NPC.transform.position) <= 10f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
